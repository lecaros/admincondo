package com.merkenlabs.admincondo



import org.junit.*
import grails.test.mixin.*

@TestFor(FineController)
@Mock(Fine)
class FineControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/fine/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.fineInstanceList.size() == 0
        assert model.fineInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.fineInstance != null
    }

    void testSave() {
        controller.save()

        assert model.fineInstance != null
        assert view == '/fine/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/fine/show/1'
        assert controller.flash.message != null
        assert Fine.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/fine/list'

        populateValidParams(params)
        def fine = new Fine(params)

        assert fine.save() != null

        params.id = fine.id

        def model = controller.show()

        assert model.fineInstance == fine
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/fine/list'

        populateValidParams(params)
        def fine = new Fine(params)

        assert fine.save() != null

        params.id = fine.id

        def model = controller.edit()

        assert model.fineInstance == fine
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/fine/list'

        response.reset()

        populateValidParams(params)
        def fine = new Fine(params)

        assert fine.save() != null

        // test invalid parameters in update
        params.id = fine.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/fine/edit"
        assert model.fineInstance != null

        fine.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/fine/show/$fine.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        fine.clearErrors()

        populateValidParams(params)
        params.id = fine.id
        params.version = -1
        controller.update()

        assert view == "/fine/edit"
        assert model.fineInstance != null
        assert model.fineInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/fine/list'

        response.reset()

        populateValidParams(params)
        def fine = new Fine(params)

        assert fine.save() != null
        assert Fine.count() == 1

        params.id = fine.id

        controller.delete()

        assert Fine.count() == 0
        assert Fine.get(fine.id) == null
        assert response.redirectedUrl == '/fine/list'
    }
}
