package com.merkenlabs.admincondo



import org.junit.*
import grails.test.mixin.*

@TestFor(ApartmentController)
@Mock(Apartment)
class ApartmentControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/apartment/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.apartmentInstanceList.size() == 0
        assert model.apartmentInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.apartmentInstance != null
    }

    void testSave() {
        controller.save()

        assert model.apartmentInstance != null
        assert view == '/apartment/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/apartment/show/1'
        assert controller.flash.message != null
        assert Apartment.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/apartment/list'

        populateValidParams(params)
        def apartment = new Apartment(params)

        assert apartment.save() != null

        params.id = apartment.id

        def model = controller.show()

        assert model.apartmentInstance == apartment
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/apartment/list'

        populateValidParams(params)
        def apartment = new Apartment(params)

        assert apartment.save() != null

        params.id = apartment.id

        def model = controller.edit()

        assert model.apartmentInstance == apartment
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/apartment/list'

        response.reset()

        populateValidParams(params)
        def apartment = new Apartment(params)

        assert apartment.save() != null

        // test invalid parameters in update
        params.id = apartment.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/apartment/edit"
        assert model.apartmentInstance != null

        apartment.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/apartment/show/$apartment.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        apartment.clearErrors()

        populateValidParams(params)
        params.id = apartment.id
        params.version = -1
        controller.update()

        assert view == "/apartment/edit"
        assert model.apartmentInstance != null
        assert model.apartmentInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/apartment/list'

        response.reset()

        populateValidParams(params)
        def apartment = new Apartment(params)

        assert apartment.save() != null
        assert Apartment.count() == 1

        params.id = apartment.id

        controller.delete()

        assert Apartment.count() == 0
        assert Apartment.get(apartment.id) == null
        assert response.redirectedUrl == '/apartment/list'
    }
}
