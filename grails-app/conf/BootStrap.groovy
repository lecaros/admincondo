import com.merkenlabs.admincondo.Owner
import com.merkenlabs.admincondo.Tenant
import com.merkenlabs.admincondo.User
import com.merkenlabs.admincondo.security.SecRole
import com.merkenlabs.admincondo.security.SecUser
import com.merkenlabs.admincondo.security.SecUserSecRole;

class BootStrap {
	def springSecurityService

    def init = { servletContext ->
		def samples =[
			'conserje1': [fullName : 'Conserje uno', email:'conserje1@admincon.cl']
			]
		
		def now = new Date()
		
		def userRole = SecRole.findByAuthority("ROLE_USER")?:new SecRole(authority:"ROLE_USER").save()
		
		def users = User.list()?:[]
		if (!users){
			samples.each { username, profileAttrs ->
				def user = new SecUser(username: username, 
					password: 'password', 
					enabled: true).save(failOnError:true)
				if(user.validate()){
					println "--------------------------------------------------------------------------------------------------------------"
					println "Creating user ${username}..."
//					user.profile = new Profile(profileAttrs)
					SecUserSecRole.create(user, userRole)
					users << user
				}
			}
		}
		def tenant = new Tenant(username: 'esoto', name: 'Elías Soto', password: 'password', title:'arrendatario')
		tenant.save(flush: true, failOnError: true)
		def owner = new Owner(username: 'rarce', name: 'Rodrigo Arce', password: 'password', title:'dueño')
		owner.save(flush: true, failInError: true)
    }
    def destroy = {
    }
}
