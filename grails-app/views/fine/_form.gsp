<%@ page import="com.merkenlabs.admincondo.Fine" %>



<div class="fieldcontain ${hasErrors(bean: fineInstance, field: 'apartment', 'error')} required">
	<label for="apartment">
		<g:message code="fine.apartment.label" default="Apartment" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="apartment" name="apartment.id" from="${com.merkenlabs.admincondo.Apartment.list()}" optionKey="id" required="" value="${fineInstance?.apartment?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: fineInstance, field: 'effectiveDate', 'error')} required">
	<label for="effectiveDate">
		<g:message code="fine.effectiveDate.label" default="Effective Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="effectiveDate" precision="day"  value="${fineInstance?.effectiveDate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: fineInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="fine.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="status" from="${com.merkenlabs.admincondo.FineStatus?.values()}" keys="${com.merkenlabs.admincondo.FineStatus.values()*.name()}" required="" value="${fineInstance?.status?.name()}"/>
</div>

