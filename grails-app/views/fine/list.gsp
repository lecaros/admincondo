
<%@ page import="com.merkenlabs.admincondo.Fine" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'fine.label', default: 'Fine')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-fine" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-fine" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="fine.apartment.label" default="Apartment" /></th>
					
						<g:sortableColumn property="effectiveDate" title="${message(code: 'fine.effectiveDate.label', default: 'Effective Date')}" />
					
						<g:sortableColumn property="status" title="${message(code: 'fine.status.label', default: 'Status')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${fineInstanceList}" status="i" var="fineInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${fineInstance.id}">${fieldValue(bean: fineInstance, field: "apartment")}</g:link></td>
					
						<td><g:formatDate date="${fineInstance.effectiveDate}" /></td>
					
						<td>${fieldValue(bean: fineInstance, field: "status")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${fineInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
