<%@ page import="com.merkenlabs.admincondo.Apartment" %>



<div class="fieldcontain ${hasErrors(bean: apartmentInstance, field: 'owner', 'error')} required">
	<label for="owner">
		<g:message code="apartment.owner.label" default="Owner" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="owner" name="owner.id" from="${com.merkenlabs.admincondo.Owner.list()}" optionKey="id" required="" value="${apartmentInstance?.owner?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: apartmentInstance, field: 'fines', 'error')} ">
	<label for="fines">
		<g:message code="apartment.fines.label" default="Fines" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${apartmentInstance?.fines?}" var="f">
    <li><g:link controller="fine" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="fine" action="create" params="['apartment.id': apartmentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'fine.label', default: 'Fine')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: apartmentInstance, field: 'numeration', 'error')} ">
	<label for="numeration">
		<g:message code="apartment.numeration.label" default="Numeration" />
		
	</label>
	<g:textField name="numeration" value="${apartmentInstance?.numeration}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: apartmentInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="apartment.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="status" from="${com.merkenlabs.admincondo.ApartmentStatus?.values()}" keys="${com.merkenlabs.admincondo.ApartmentStatus.values()*.name()}" required="" value="${apartmentInstance?.status?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: apartmentInstance, field: 'tenancyAgreements', 'error')} ">
	<label for="tenancyAgreements">
		<g:message code="apartment.tenancyAgreements.label" default="Tenancy Agreements" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${apartmentInstance?.tenancyAgreements?}" var="t">
    <li><g:link controller="tenancyAgreement" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="tenancyAgreement" action="create" params="['apartment.id': apartmentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'tenancyAgreement.label', default: 'TenancyAgreement')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: apartmentInstance, field: 'tenant', 'error')} required">
	<label for="tenant">
		<g:message code="apartment.tenant.label" default="Tenant" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tenant" name="tenant.id" from="${com.merkenlabs.admincondo.Tenant.list()}" optionKey="id" required="" value="${apartmentInstance?.tenant?.id}" class="many-to-one"/>
</div>

