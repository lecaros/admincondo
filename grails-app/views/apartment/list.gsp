
<%@ page import="com.merkenlabs.admincondo.Apartment" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'apartment.label', default: 'Apartment')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-apartment" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-apartment" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="apartment.owner.label" default="Owner" /></th>
					
						<g:sortableColumn property="numeration" title="${message(code: 'apartment.numeration.label', default: 'Numeration')}" />
					
						<g:sortableColumn property="status" title="${message(code: 'apartment.status.label', default: 'Status')}" />
					
						<th><g:message code="apartment.tenant.label" default="Tenant" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${apartmentInstanceList}" status="i" var="apartmentInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${apartmentInstance.id}">${fieldValue(bean: apartmentInstance, field: "owner")}</g:link></td>
					
						<td>${fieldValue(bean: apartmentInstance, field: "numeration")}</td>
					
						<td>${fieldValue(bean: apartmentInstance, field: "status")}</td>
					
						<td>${fieldValue(bean: apartmentInstance, field: "tenant")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${apartmentInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
