
<%@ page import="com.merkenlabs.admincondo.Apartment" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'apartment.label', default: 'Apartment')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-apartment" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-apartment" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list apartment">
			
				<g:if test="${apartmentInstance?.owner}">
				<li class="fieldcontain">
					<span id="owner-label" class="property-label"><g:message code="apartment.owner.label" default="Owner" /></span>
					
						<span class="property-value" aria-labelledby="owner-label"><g:link controller="owner" action="show" id="${apartmentInstance?.owner?.id}">${apartmentInstance?.owner?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${apartmentInstance?.fines}">
				<li class="fieldcontain">
					<span id="fines-label" class="property-label"><g:message code="apartment.fines.label" default="Fines" /></span>
					
						<g:each in="${apartmentInstance.fines}" var="f">
						<span class="property-value" aria-labelledby="fines-label"><g:link controller="fine" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${apartmentInstance?.numeration}">
				<li class="fieldcontain">
					<span id="numeration-label" class="property-label"><g:message code="apartment.numeration.label" default="Numeration" /></span>
					
						<span class="property-value" aria-labelledby="numeration-label"><g:fieldValue bean="${apartmentInstance}" field="numeration"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apartmentInstance?.status}">
				<li class="fieldcontain">
					<span id="status-label" class="property-label"><g:message code="apartment.status.label" default="Status" /></span>
					
						<span class="property-value" aria-labelledby="status-label"><g:fieldValue bean="${apartmentInstance}" field="status"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${apartmentInstance?.tenancyAgreements}">
				<li class="fieldcontain">
					<span id="tenancyAgreements-label" class="property-label"><g:message code="apartment.tenancyAgreements.label" default="Tenancy Agreements" /></span>
					
						<g:each in="${apartmentInstance.tenancyAgreements}" var="t">
						<span class="property-value" aria-labelledby="tenancyAgreements-label"><g:link controller="tenancyAgreement" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${apartmentInstance?.tenant}">
				<li class="fieldcontain">
					<span id="tenant-label" class="property-label"><g:message code="apartment.tenant.label" default="Tenant" /></span>
					
						<span class="property-value" aria-labelledby="tenant-label"><g:link controller="tenant" action="show" id="${apartmentInstance?.tenant?.id}">${apartmentInstance?.tenant?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${apartmentInstance?.id}" />
					<g:link class="edit" action="edit" id="${apartmentInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
