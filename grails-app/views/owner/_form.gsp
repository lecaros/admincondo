<%@ page import="com.merkenlabs.admincondo.Owner" %>



<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="owner.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${ownerInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="owner.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${ownerInstance?.password}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'accountExpired', 'error')} ">
	<label for="accountExpired">
		<g:message code="owner.accountExpired.label" default="Account Expired" />
		
	</label>
	<g:checkBox name="accountExpired" value="${ownerInstance?.accountExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'accountLocked', 'error')} ">
	<label for="accountLocked">
		<g:message code="owner.accountLocked.label" default="Account Locked" />
		
	</label>
	<g:checkBox name="accountLocked" value="${ownerInstance?.accountLocked}" />
</div>

<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'apartments', 'error')} ">
	<label for="apartments">
		<g:message code="owner.apartments.label" default="Apartments" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${ownerInstance?.apartments?}" var="a">
    <li><g:link controller="apartment" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="apartment" action="create" params="['owner.id': ownerInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'apartment.label', default: 'Apartment')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="owner.enabled.label" default="Enabled" />
		
	</label>
	<g:checkBox name="enabled" value="${ownerInstance?.enabled}" />
</div>

<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="owner.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${ownerInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'passwordExpired', 'error')} ">
	<label for="passwordExpired">
		<g:message code="owner.passwordExpired.label" default="Password Expired" />
		
	</label>
	<g:checkBox name="passwordExpired" value="${ownerInstance?.passwordExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: ownerInstance, field: 'title', 'error')} ">
	<label for="title">
		<g:message code="owner.title.label" default="Title" />
		
	</label>
	<g:textField name="title" value="${ownerInstance?.title}"/>
</div>

