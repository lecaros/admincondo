
<%@ page import="com.merkenlabs.admincondo.Owner" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'owner.label', default: 'Owner')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-owner" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-owner" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list owner">
			
				<g:if test="${ownerInstance?.username}">
				<li class="fieldcontain">
					<span id="username-label" class="property-label"><g:message code="owner.username.label" default="Username" /></span>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${ownerInstance}" field="username"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ownerInstance?.password}">
				<li class="fieldcontain">
					<span id="password-label" class="property-label"><g:message code="owner.password.label" default="Password" /></span>
					
						<span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${ownerInstance}" field="password"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ownerInstance?.accountExpired}">
				<li class="fieldcontain">
					<span id="accountExpired-label" class="property-label"><g:message code="owner.accountExpired.label" default="Account Expired" /></span>
					
						<span class="property-value" aria-labelledby="accountExpired-label"><g:formatBoolean boolean="${ownerInstance?.accountExpired}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${ownerInstance?.accountLocked}">
				<li class="fieldcontain">
					<span id="accountLocked-label" class="property-label"><g:message code="owner.accountLocked.label" default="Account Locked" /></span>
					
						<span class="property-value" aria-labelledby="accountLocked-label"><g:formatBoolean boolean="${ownerInstance?.accountLocked}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${ownerInstance?.apartments}">
				<li class="fieldcontain">
					<span id="apartments-label" class="property-label"><g:message code="owner.apartments.label" default="Apartments" /></span>
					
						<g:each in="${ownerInstance.apartments}" var="a">
						<span class="property-value" aria-labelledby="apartments-label"><g:link controller="apartment" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${ownerInstance?.enabled}">
				<li class="fieldcontain">
					<span id="enabled-label" class="property-label"><g:message code="owner.enabled.label" default="Enabled" /></span>
					
						<span class="property-value" aria-labelledby="enabled-label"><g:formatBoolean boolean="${ownerInstance?.enabled}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${ownerInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="owner.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${ownerInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ownerInstance?.passwordExpired}">
				<li class="fieldcontain">
					<span id="passwordExpired-label" class="property-label"><g:message code="owner.passwordExpired.label" default="Password Expired" /></span>
					
						<span class="property-value" aria-labelledby="passwordExpired-label"><g:formatBoolean boolean="${ownerInstance?.passwordExpired}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${ownerInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="owner.title.label" default="Title" /></span>
					
						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${ownerInstance}" field="title"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${ownerInstance?.id}" />
					<g:link class="edit" action="edit" id="${ownerInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
