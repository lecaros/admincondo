package com.merkenlabs.admincondo

import org.springframework.dao.DataIntegrityViolationException

class ApartmentController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [apartmentInstanceList: Apartment.list(params), apartmentInstanceTotal: Apartment.count()]
    }

    def create() {
        [apartmentInstance: new Apartment(params)]
    }

    def save() {
        def apartmentInstance = new Apartment(params)
        if (!apartmentInstance.save(flush: true)) {
            render(view: "create", model: [apartmentInstance: apartmentInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'apartment.label', default: 'Apartment'), apartmentInstance.id])
        redirect(action: "show", id: apartmentInstance.id)
    }

    def show(Long id) {
        def apartmentInstance = Apartment.get(id)
        if (!apartmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'apartment.label', default: 'Apartment'), id])
            redirect(action: "list")
            return
        }

        [apartmentInstance: apartmentInstance]
    }

    def edit(Long id) {
        def apartmentInstance = Apartment.get(id)
        if (!apartmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'apartment.label', default: 'Apartment'), id])
            redirect(action: "list")
            return
        }

        [apartmentInstance: apartmentInstance]
    }

    def update(Long id, Long version) {
        def apartmentInstance = Apartment.get(id)
        if (!apartmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'apartment.label', default: 'Apartment'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (apartmentInstance.version > version) {
                apartmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'apartment.label', default: 'Apartment')] as Object[],
                          "Another user has updated this Apartment while you were editing")
                render(view: "edit", model: [apartmentInstance: apartmentInstance])
                return
            }
        }

        apartmentInstance.properties = params

        if (!apartmentInstance.save(flush: true)) {
            render(view: "edit", model: [apartmentInstance: apartmentInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'apartment.label', default: 'Apartment'), apartmentInstance.id])
        redirect(action: "show", id: apartmentInstance.id)
    }

    def delete(Long id) {
        def apartmentInstance = Apartment.get(id)
        if (!apartmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'apartment.label', default: 'Apartment'), id])
            redirect(action: "list")
            return
        }

        try {
            apartmentInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'apartment.label', default: 'Apartment'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'apartment.label', default: 'Apartment'), id])
            redirect(action: "show", id: id)
        }
    }
}
