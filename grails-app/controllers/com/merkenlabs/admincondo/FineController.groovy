package com.merkenlabs.admincondo

import grails.plugins.springsecurity.Secured;

import org.springframework.dao.DataIntegrityViolationException

class FineController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	@Secured("ROLE_USER")
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [fineInstanceList: Fine.list(params), fineInstanceTotal: Fine.count()]
    }

    def create() {
        [fineInstance: new Fine(params)]
    }

    def save() {
        def fineInstance = new Fine(params)
        if (!fineInstance.save(flush: true)) {
            render(view: "create", model: [fineInstance: fineInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'fine.label', default: 'Fine'), fineInstance.id])
        redirect(action: "show", id: fineInstance.id)
    }

    def show(Long id) {
        def fineInstance = Fine.get(id)
        if (!fineInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fine.label', default: 'Fine'), id])
            redirect(action: "list")
            return
        }

        [fineInstance: fineInstance]
    }

    def edit(Long id) {
        def fineInstance = Fine.get(id)
        if (!fineInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fine.label', default: 'Fine'), id])
            redirect(action: "list")
            return
        }

        [fineInstance: fineInstance]
    }

    def update(Long id, Long version) {
        def fineInstance = Fine.get(id)
        if (!fineInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fine.label', default: 'Fine'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (fineInstance.version > version) {
                fineInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'fine.label', default: 'Fine')] as Object[],
                          "Another user has updated this Fine while you were editing")
                render(view: "edit", model: [fineInstance: fineInstance])
                return
            }
        }

        fineInstance.properties = params

        if (!fineInstance.save(flush: true)) {
            render(view: "edit", model: [fineInstance: fineInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'fine.label', default: 'Fine'), fineInstance.id])
        redirect(action: "show", id: fineInstance.id)
    }

    def delete(Long id) {
        def fineInstance = Fine.get(id)
        if (!fineInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fine.label', default: 'Fine'), id])
            redirect(action: "list")
            return
        }

        try {
            fineInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'fine.label', default: 'Fine'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'fine.label', default: 'Fine'), id])
            redirect(action: "show", id: id)
        }
    }
}
