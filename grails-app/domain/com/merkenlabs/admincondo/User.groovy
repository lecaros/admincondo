package com.merkenlabs.admincondo

import com.merkenlabs.admincondo.security.SecUser

class User extends SecUser {
	
	String name
	String title
	
//	static hasMany =[apartments: Apartment, tenancyAgreements: TenancyAgreement]

    static constraints = {
		username(blank: false, unique: true)
    }
}
