package com.merkenlabs.admincondo

class Apartment {
	String numeration
	ApartmentStatus status
	
	static hasOne=[owner: Owner, tenant: Tenant]
	static hasMany=[tenancyAgreements: TenancyAgreement, fines: Fine]
	
    static constraints = {
		owner blank:false
    }
}
