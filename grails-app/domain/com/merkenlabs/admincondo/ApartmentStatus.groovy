package com.merkenlabs.admincondo

import org.springframework.context.MessageSourceResolvable;

enum ApartmentStatus implements MessageSourceResolvable{
	
	AVAILABLE, OCCUPIED
	
	public Object[] getArguments() {[]as Object}
	
	public String[] getCodes(){[name()]}
	
	public String getDefaultMessage() {"--" + name()}
}
