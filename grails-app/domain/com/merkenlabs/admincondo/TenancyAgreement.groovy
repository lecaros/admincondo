package com.merkenlabs.admincondo

class TenancyAgreement {
	Date startDate
	Date endDate
	int contractNumber
	
	static hasOne = [tenant: Tenant, apartment: Apartment]

    static constraints = {
    }
}
