package com.merkenlabs.admincondo;

import org.springframework.context.MessageSourceResolvable;

enum FineStatus implements MessageSourceResolvable{
	
	ISSUED, DELIVERED, CANCELLED, PAID
	
	public Object[] getArguments() {[]as Object}
	
	public String[] getCodes(){[name()]}
	
	public String getDefaultMessage() {"--" + name()}

}
