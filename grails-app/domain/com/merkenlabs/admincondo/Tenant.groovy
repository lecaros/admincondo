package com.merkenlabs.admincondo

class Tenant extends User{
	
	static hasMany = [apartments: Apartment, tenancyAgreements: TenancyAgreement]

    static constraints = {
    }
	
	@Override public String toString() {
		return name
	  }
	
}
